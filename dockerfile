FROM csirocass/askapsoft:1.12.0-openmpi4

USER root
RUN apt-get update && apt-get install --yes sssd && apt-get clean
ADD nsswitch.conf /etc/
